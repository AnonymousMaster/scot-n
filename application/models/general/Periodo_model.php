<?php

class Periodo_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    // Reporte_ctrllr -> control_lectura_redirigir
    public function get_one_periodo($idPeriodo) {
        $query = $this->db->query('SELECT * FROM "Periodo" '
                . 'WHERE "PrdId" = ?', array($idPeriodo));
        return $query->row_array();
    }
    
    // CronogramaLectura_ctrllr -> cronograma_mensual_nuevo
    public function get_one_periodo_x_orden($orden, $anio) {
        $query = $this->db->query('SELECT * FROM "Periodo" '
                . 'WHERE "PrdOrd" = ? AND "PrdAni" = ? ORDER BY "PrdOrd" DESC LIMIT 1', array($orden, $anio));
        return $query->row_array();
    }

    // Reporte_ctrllr -> control_lectura
    public function get_last_periodo_x_orden($orden, $anio) {
        $query = $this->db->query('SELECT * FROM "Periodo" '
                . 'WHERE "PrdOrd" <= ? AND "PrdAni" = ? ORDER BY "PrdOrd" DESC LIMIT 1', array($orden, $anio));
        return $query->row_array();
    }
    
    public function get_all_periodo($idActividad, $anio, $idContratante) {
        $query = $this->db->query('SELECT * FROM "Periodo" '
                . 'WHERE "PrdEli" = FALSE AND "PrdActId" = ? AND "PrdAni" = ? ORDER BY "PrdOrd" ASC', array($idActividad, $anio));
        return $query->result_array();
    }

    // CronogramaLEctura_ctrllr -> cronograma_anual_nuevo
    public function get_ultimo_periodo($idActividad, $anio, $idContratante) {
        $query = $this->db->query('SELECT * FROM "Periodo" '
                . 'WHERE "PrdEli" = FALSE AND "PrdActId" = ? AND "PrdAni" = ? ORDER BY "PrdOrd" DESC LIMIT 1', array($idActividad, ($anio-1)));
        return $query->row_array();
    }
    
    //Reporte_ctrllr -> control
    public function get_anios($idContratante, $idActividad) {
        $query = $this->db->query('SELECT DISTINCT "PrdAni" FROM "Orden_trabajo"
        JOIN  "Periodo"  ON "OrtPrdId" = "PrdId"
        WHERE "OrtCntId" = ?', array($idContratante));
        return array_column($query->result_array(), 'PrdAni');
    }
    
    //Reporte_ctrllr -> control
    public function get_ordenes_x_anio($idContratante, $idActividad, $anio) {
        $query = $this->db->query('SELECT DISTINCT "PrdOrd", "PrdCod" FROM "Periodo" 
        WHERE "PrdCntId" = ? AND "PrdActId" = ? AND "PrdAni" = ? ORDER BY "PrdOrd" ASC', array($idContratante, $idActividad, $anio));
        return array_column($query->result_array(),'PrdCod', 'PrdOrd');
    }

//    public function insert_unidad($descripcion) {
//        $query = $this->db->query('INSERT into "Unidad_medida" ("UniDes", "UniEli", "UniFchRg", "UniFchAc") VALUES (?,?,?,?); '
//                , array($descripcion, FALSE, date("Y-m-d H:i:s"), date("Y-m-d H:i:s")));
//        return $query;
//    }
//
//    public function update_unidad($idUnidad, $descripcion) {
//        $query = $this->db->query('UPDATE "Unidad_medida" SET "UniDes" = ?, "UniFchAc" = ? WHERE "UniId" = ? ;'
//                , array($descripcion, date("Y-m-d H:i:s"), $idUnidad));
//        return $query;
//    }
//
//    public function delete_unidad($idUnidad) {
//        $query = $this->db->query('UPDATE "Unidad_medida" SET "UniEli" = TRUE, "UniFchAc" = ? '
//                . 'WHERE "UniId" = ?', array(date("Y-m-d H:i:s"), $idUnidad));
//        return $query;
//    }
}
